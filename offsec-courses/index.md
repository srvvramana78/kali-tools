---
Title: offsec-courses
Homepage: https://www.kali.org
Repository: https://gitlab.com/kalilinux/packages/offsec-courses
Architectures: any
Version: 2021.1.2
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### offsec-awae
 
  This is Kali Linux, the most advanced penetration testing and security
  auditing distribution.
   
  This metapackage depends on the resources required for OffSec's
  AWAE/WEB-300/OSWE.
 
 **Installed size:** `34 KB`  
 **How to install:** `sudo apt install offsec-awae`  
 
 {{< spoiler "Dependencies:" >}}
 * burpsuite
 * firefox-esr
 * freerdp2-x11
 * impacket-scripts
 * kali-linux-core
 * netcat-traditional
 * offsec-awae-python2
 * openjdk-11-jdk-headless
 * rdesktop
 {{< /spoiler >}}
 
 
 - - -
 
 ### offsec-awae-python2
 
  This package provides the Python 2 resources for OffSec's AWAE, and it depends
  on the resources required for OffSec's AWAE/WEB-300/OSWE.
 
 **Installed size:** `1.60 MB`  
 **How to install:** `sudo apt install offsec-awae-python2`  
 
 {{< spoiler "Dependencies:" >}}
 * ca-certificates
 * python-cffi 
 * python2
 {{< /spoiler >}}
 
 
 - - -
 
 ### offsec-exp301
 
  This is Kali Linux, the most advanced penetration testing and security
  auditing distribution.
   
  This metapackage depends on the resources required for OffSec's
  WUMED/EXP-301/OSED.
 
 **Installed size:** `34 KB`  
 **How to install:** `sudo apt install offsec-exp301`  
 
 {{< spoiler "Dependencies:" >}}
 * impacket-scripts
 * kali-linux-core
 * metasploit-framework
 * python3
 * rdesktop
 {{< /spoiler >}}
 
 
 - - -
 
 ### offsec-pen300
 
  This is Kali Linux, the most advanced penetration testing and security
  auditing distribution.
   
  This metapackage depends on the resources required for OffSec's
  ETBD/PEN-300/OSEP.
 
 **Installed size:** `34 KB`  
 **How to install:** `sudo apt install offsec-pen300`  
 
 {{< spoiler "Dependencies:" >}}
 * apache2
 * chisel
 * creddump7
 * dnscat2
 * firefox-esr | firefox | www-browser
 * freerdp2-x11
 * gcc
 * gobuster
 * golang
 * hashcat
 * impacket-scripts
 * john
 * kali-linux-core
 * krb5-user
 * metasploit-framework
 * nmap
 * openssh-client
 * openssh-server
 * powershell
 * proxychains4
 * python3
 * rdesktop
 * responder
 * samba
 * tigervnc-viewer
 * wireshark
 {{< /spoiler >}}
 
 
 - - -
 
 ### offsec-pwk
 
  This is Kali Linux, the most advanced penetration testing and security
  auditing distribution.
   
  This metapackage depends on the resources required for OffSec's
  PWK2/PEN-200/OSCP.
 
 **Installed size:** `34 KB`  
 **How to install:** `sudo apt install offsec-pwk`  
 
 {{< spoiler "Dependencies:" >}}
 * apache2
 * atftp
 * axel
 * burpsuite
 * busybox
 * cewl
 * crowbar
 * crunch
 * curl
 * cutycapt
 * default-libmysqlclient-dev
 * dirb
 * dnsenum
 * dnsrecon
 * enum4linux
 * exe2hexbat
 * exploitdb
 * firefox-esr
 * freerdp2-x11
 * hashcat
 * hashid
 * httptunnel
 * hydra
 * iptables
 * john
 * kali-linux-core
 * kali-tools-windows-resources
 * kerberoast
 * leafpad
 * masscan
 * medusa
 * metasploit-framework
 * mimikatz
 * mingw-w64
 * nano
 * nbtscan
 * netcat
 * nikto
 * nmap
 * onesixtyone
 * openssl
 * openvpn
 * passing-the-hash
 * powercat
 * powershell-empire
 * proxychains
 * pure-ftpd
 * python3
 * rdesktop
 * recon-ng
 * rinetd
 * seclists
 * shellter
 * snmp
 * socat
 * sqlmap
 * tcpdump
 * theharvester
 * wce
 * wget
 * whois
 * wine
 * wireshark
 * wpscan
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
